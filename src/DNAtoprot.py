#mise au propre: 

import sys

def rev_complementaire(seq):
    dico_complementaire= { "A": "T", "T": "A", "C": "G", "G": "C"}
    seq_rc = ""
    for base in seq:
        seq_rc += dico_complementaire[base]
    return seq_rc

#la fonction rev_complementaire permet d'obetenir la sequence reverse complementaire de la sequence donnée en input 


def cadres_lectures (seq):
	cadre_lecture_1 = []
	cadre_lecture_2 = []
	cadre_lecture_3 = []
	for i in range (0, len(seq)-2,3):
		codon = seq[i:i+3]
		cadre_lecture_1.append(codon)
	
	for i in range (1, len (seq)-2,3):
		codon = seq[i:i+3]
		cadre_lecture_2.append(codon)
	
	for i in range (2, len (seq)-2,3):
		codon = seq[i:i+3]
		cadre_lecture_3.append(codon)
	return [cadre_lecture_1, cadre_lecture_2, cadre_lecture_3]

#la fonction cadres_lectures permet donner les codons des 3 cadres de lectures possible pour la sequence donnée en input

version = sys.argv[2]
def traduction (seq, version):
    dico_traduc= {"GCT": "A", 
              "GCC": "A",
              "GCA": "A",
              "GCG": "A",
              "CGT": "R",
              "CGC": "R",
              "CGA": "R",
              "CGG": "R",
              "AGA": "R",
              "AGG": "R",
              "AAT": "N",
              "AAC": "N",
              "GAT": "D",
              "GAC": "D",
              "TGT": "C",
              "TGC": "C",
              "CAA": "Q",
              "CAG": "Q",
              "GAA": "E",
              "GAG": "E",
              "GGT": "G",
              "GGC": "G",
              "GGA": "G",
              "GGG": "G",
              "CAT": "H",
              "CAC": "H",
              "ATT": "I",
              "ATC": "I",
              "ATA": "I",
              "TTA": "L",
              "TTG": "L",
              "CTT": "L",
              "CTC": "L",
              "CTA": "L",
              "CTG": "L",
              "AAA": "K",
              "AAG": "K",
              "ATG": "M",
              "TTT": "F",
              "TTC": "F",
              "CCT": "P",
              "CCC": "P",
              "CCA": "P",
              "CCG": "P",
              "TCT": "S",
              "TCC": "S",
              "TCA": "S",
              "TCG": "S",
              "AGT": "S",
              "AGC": "S",
              "ACT": "T",
              "ACC": "T",
              "ACA": "T",
              "ACG": "T",
              "TGG": "W",
              "TAT": "Y",
              "TAC": "Y",
              "GTT": "V",
              "GTC": "V",
              "GTA": "V",
              "GTG": "V",
              "TAG": "_",
              "TGA": "_",
              "TAA": "_"}
    seq_trad = ""			
    for codon in seq:
        seq_trad += dico_traduc[codon]
        seq_trad = seq_trad.replace("_", "\n")
    return seq_trad


    if version== "mycoplasme":
        dico_traduc["TGA"]="W"
        seq_trad = ""
        for codon in seq:
            seq_trad += dico_traduc[codon]
            seq_trad = seq_trad.replace("_", "\n")
        return seq_trad

#la fonction traduction permet de traduire en AA les codons de la sequence donnée en input


def ecriture (ma_liste, mon_fichier):
    with open (mon_fichier, "w") as fichier:
        for item in ma_liste:
            fichier.write(item)

#la fonction ecriture permet d'écrire un fichier contenant les données contenues dans la liste donnée en input. il faut 
#egalement lui donner le nom que l'on souhaite donner au fichier en question.


def traduction_cadres(seq, tag):
    liste_cadres_lectures = cadres_lectures(seq)
    sequence_codon = []
    count1 = 1
    for cadre in liste_cadres_lectures:
        if count1 > 0:
            trad = traduction(cadre, version)
            sequence_codon.append(trad)
            mes_elements = fichier_input.split(".")
            mon_fichier = mes_elements[0] + tag + str(count1) + "_prot." + mes_elements[1]
            ecriture(sequence_codon, mon_fichier)
            sequence_codon = []
        count1 += 1

#la fonction traduction_cadres permet d'obtenir la séqeunce en AA pour les 3 cadres de lectures de la sequence donnée en input
#elle permet egalement d'ecrire un fichier contenant chacun une phase de lecture.


sequence_fasta = ""

fichier_input = sys.argv[1]

#with open (fichier_input,"r") as filein:
#    lignes = filein.readlines()
#    lignes_sans_premiere = lignes[1:]
#    for ligne in lignes_sans_premiere:
#        sequence_fasta.append(ligne)
# 

count = 0

with open (fichier_input,"r") as filein:
    for ligne in filein:
        if count > 0 :
            sequence_fasta += ligne.strip("\n")  
        count += 1
sequence_fasta = sequence_fasta[1:]
 
#print(sequence_fasta)

sequence_fasta_rc= rev_complementaire(sequence_fasta)
##print(sequence_fasta_rc)


traduction_cadres(sequence_fasta, "")

traduction_cadres(sequence_fasta_rc, "rc")
