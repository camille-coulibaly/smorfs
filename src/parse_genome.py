#Copyright Camille Coulibaly, Université de Paris
#contributor(s) : Camille Coulibaly, Guillaume Postic (2020)
#
#camille.coulibaly@etu.u-paris.fr
#
#This software is a computer program whose purpose is to detect smORFs
#in prokaryotic and eukaryotic genomes.
#
#This software is governed by the CeCILL  license under French law and
#abiding by the rules of distribution of free software.  You can  use, 
#modify and/ or redistribute the software under the terms of the CeCILL
#license as circulated by CEA, CNRS and INRIA at the following URL
#"http://www.cecill.info". 
#
#As a counterpart to the access to the source code and  rights to copy,
#modify and redistribute granted by the license, users are provided only
#with a limited warranty  and the software's author,  the holder of the
#economic rights,  and the successive licensors  have only  limited
#liability. 
#
#In this respect, the user's attention is drawn to the risks associated
#with loading,  using,  modifying and/or developing or reproducing the
#software by the user in light of its specific status of free software,
#that may mean  that it is complicated to manipulate,  and  that  also
#therefore means  that it is reserved for developers  and  experienced
#professionals having in-depth computer knowledge. Users are therefore
#encouraged to load and test the software's suitability as regards their
#requirements in conditions enabling the security of their systems and/or 
#data to be ensured and,  more generally, to use and operate it in the 
#same conditions as regards security. 
#
#The fact that you are presently reading this means that you have had
#knowledge of the CeCILL license and that you accept its terms.

import sys

fichier_input = sys.argv[1]


def ecriture (ma_liste, mon_fichier):
    with open (mon_fichier, "w") as fichier:
        for item in ma_liste:
            fichier.write(item)

liste_genome = []

count = 0  #initialisation du compteur "count" a zero
compteur = 0  #initialisation du compteur "compteur" a zero

with open (fichier_input, "r") as filein:  #ouvre le fichier input en mode lecture
    for ligne in filein:  #pour chaque ligne dans le fichier input  
        liste_genome.append(ligne)  #la liste liste_genome est incrementee de la ligne
        if ligne[0]==">" and compteur > 0: #si la ligne commence par ">" et si le competeur "compteur" est superieur à 0 (correspond à la premiere ligne).
            liste_genome = liste_genome[:-1]  #la liste_genome perd sa derniere ligne qui correspond à la ligne commencant par ">" du genome suivant
            mes_elements = fichier_input.split(".")   #coupe en 2 le nom du fichier input au niveau du "."
            mon_fichier= mes_elements[0]+str(count)+"."+mes_elements[1] #determine le nom du fichier
            ecriture(liste_genome, mon_fichier) # permet d'ecrire le contenu de la liste_genome dans le nouveau fichier
            count += 1 #le compteur "count" comptant le nb de genome dans le fichier prend +1
            liste_genome = [ligne] # vidage et reinitialisation de la liste_genome
        compteur += 1 # le compteur  "compteur" comptant le nb de ligne prend +1 avant de passer à la ligne suivante.
    mon_fichier = mes_elements[0]+str(count)+"."+mes_elements[1] 
    ecriture(liste_genome, mon_fichier)  #pour imprimer le dernier genome dans un fichier, on fait appel à la fonction ecriture en dehors et a la suite de la boucle for. 

