#Copyright Camille Coulibaly, Université de Paris
#contributor(s) : Camille Coulibaly, Guillaume Postic (2020)
#
#camille.coulibaly@etu.u-paris.fr
#
#This software is a computer program whose purpose is to detect smORFs
#in prokaryotic and eukaryotic genomes.
#
#This software is governed by the CeCILL  license under French law and
#abiding by the rules of distribution of free software.  You can  use, 
#modify and/ or redistribute the software under the terms of the CeCILL
#license as circulated by CEA, CNRS and INRIA at the following URL
#"http://www.cecill.info". 
#
#As a counterpart to the access to the source code and  rights to copy,
#modify and redistribute granted by the license, users are provided only
#with a limited warranty  and the software's author,  the holder of the
#economic rights,  and the successive licensors  have only  limited
#liability. 
#
#In this respect, the user's attention is drawn to the risks associated
#with loading,  using,  modifying and/or developing or reproducing the
#software by the user in light of its specific status of free software,
#that may mean  that it is complicated to manipulate,  and  that  also
#therefore means  that it is reserved for developers  and  experienced
#professionals having in-depth computer knowledge. Users are therefore
#encouraged to load and test the software's suitability as regards their
#requirements in conditions enabling the security of their systems and/or 
#data to be ensured and,  more generally, to use and operate it in the 
#same conditions as regards security. 
#
#The fact that you are presently reading this means that you have had
#knowledge of the CeCILL license and that you accept its terms.

def get_args():
    parser = argparse.ArgumentParser(description = 'MS2MODELS wrapper')

    parser.add_argument('-i', dest='infile', type=str, help="the input multi-FASTA file", required=True)
    parser.add_argument('-v', dest='fold_change', type=str, help="the optional fold change table")
    parser.add_argument('-nmax', dest='tasks', type=int, help = "maximum size threshold of the ORF")
    parser.add_argument('-nmin', dest = 'tasks1', type=int, help = "minimum size threshold of the ORF")
    parser.add_argument('-start', dest = 'startprot', type=str, help = "choose the first AA of proteins")

    if len(sys.argv) < 2:
        parser.print_help() # méthode de argparse pour afficher l'aide
        sys.exit(1) # on arrête le programme

    args = parser.parse_args()

 
    return args.infile, args.fold_change, args.tasks, args.tasks1, args.startprot


    print("valeur donnée à l'option i = "+fichier_input)


def rev_complementaire(seq):
    """
    input : sequence d'interet
    output : sequence reverse complementaire de la sequence donnee en input
    """
    dico_complementaire= { "A": "T", "T": "A", "C": "G", "G": "C", "N":"N"}
    seq_rc = ""
    for base in seq:
        seq_rc += dico_complementaire[base]
    return seq_rc

 

def cadres_lectures (seq):
    """
    input: sequence d'interet
    output: 3 listes de codons, correspondant au 3 cadres de lectures possibles pour la sequence donnees en input.
    """
    cadre_lecture_1 = []
    cadre_lecture_2 = []
    cadre_lecture_3 = []
    for i in range (0, len(seq)-2,3):
        codon = seq[i:i+3]
        cadre_lecture_1.append(codon)
	
    for i in range (1, len (seq)-2,3):
        codon = seq[i:i+3]
        cadre_lecture_2.append(codon)
	
    for i in range (2, len (seq)-2,3):
        codon = seq[i:i+3]
        cadre_lecture_3.append(codon)

    return [cadre_lecture_1, cadre_lecture_2, cadre_lecture_3]



def traduction (seq, version, nmax, nmin, start):
    """
    input : sequence d'interet + la version du code genetique utilise
    output: retourne la sequence traduite en AA
    """
    dico_traduc= {"GCT": "A", 
              "GCC": "A",
              "GCA": "A",
              "GCG": "A",
              "CGT": "R",
              "CGC": "R",
              "CGA": "R",
              "CGG": "R",
              "AGA": "R",
              "AGG": "R",
              "AAT": "N",
              "AAC": "N",
              "GAT": "D",
              "GAC": "D",
              "TGT": "C",
              "TGC": "C",
              "CAA": "Q",
              "CAG": "Q",
              "GAA": "E",
              "GAG": "E",
              "GGT": "G",
              "GGC": "G",
              "GGA": "G",
              "GGG": "G",
              "CAT": "H",
              "CAC": "H",
              "ATT": "I",
              "ATC": "I",
              "ATA": "I",
              "TTA": "L",
              "TTG": "L",
              "CTT": "L",
              "CTC": "L",
              "CTA": "L",
              "CTG": "L",
              "AAA": "K",
              "AAG": "K",
              "ATG": "M",
              "TTT": "F",
              "TTC": "F",
              "CCT": "P",
              "CCC": "P",
              "CCA": "P",
              "CCG": "P",
              "TCT": "S",
              "TCC": "S",
              "TCA": "S",
              "TCG": "S",
              "AGT": "S",
              "AGC": "S",
              "ACT": "T",
              "ACC": "T",
              "ACA": "T",
              "ACG": "T",
              "TGG": "W",
              "TAT": "Y",
              "TAC": "Y",
              "GTT": "V",
              "GTC": "V",
              "GTA": "V",
              "GTG": "V",
              "TAG": "_",
              "TGA": "_",
              "TAA": "_"}

    if version== "mycoplasme":
        dico_traduc["TGA"]="W"

    #if "N" in codon : 
    #    aa = "X"

    seq_trad = ""
    premier_aa = ""
    aa_avant = ""
    liste_prot = "" #variable tampon
    cmpt = 0  #compteur d'AA 
    for codon in seq:
        cmpt += 1    #le compteur d'acide amine est incremente de 1
        aa = dico_traduc[codon] 
        if start == None:
            start = aa 
        if aa_avant == "" or aa_avant == "_": #si l'aa precedent celui traite est nul ou egal à un codon stop
            premier_aa = aa #le premier aa de la proteine est egale a l'aa traite dans la boucle
        if premier_aa == start:  #si le premier aa = le aa determine par start
            liste_prot += aa #pour chaque codon (triplet), la variable tampon s'allonge de 1 AA
        if nmin < cmpt and cmpt < nmax  and aa == "_": #si la longueur de la proteine est superieure a nmin AA et  inferieure a nmax AA ET si on tombe sur un codon stop 
            liste_prot = liste_prot.replace("_", "\n") #on remplace les _ par des retours à la ligne
            cmpt = 0 # le compteur d'aa est remis a 0
            seq_trad += liste_prot #la sequence traduite prend la valeur de la liste_prot
            liste_prot="" #la variable tampon , liste_prot est reinitialisee
            premier_aa = "" # tout comme premier_aa
        aa_avant = aa #la variable aa_avant prend la valeur de l'aa traite dans cette boucle.
 
    return seq_trad




def ecriture (ma_liste, mon_fichier):
    """
    input : liste contenant ce que l'on veut mettre dans un fichier + le nom du futur fichier
    output : fichier contenant les elements de la liste donnee en input
    """
    with open (mon_fichier, "w") as fichier:
        for item in ma_liste:
            fichier.write(item)



def traduction_cadres(seq,fichier_input, tag, version, nmax, nmin, start):
    """
    input : sequence d'interet, le fichier_input pour determiner le nom du fichier_output, un tag pour differencier les noms de fichiers entre la sequence dorigine et la sequence reverse complementaire, et la version du code genetique utilise
    output:produit un fichier  pour chaque phase de lecture contenant les sequences d'AA
    """
    liste_cadres_lectures = cadres_lectures(seq)
    sequence_codon = []
    count1 = 1
    for cadre in liste_cadres_lectures:
        if count1 > 0:
            trad = traduction(cadre, version, nmax, nmin, start)
            sequence_codon.append(trad)
            mes_elements = fichier_input.split(".")
            mon_fichier = mes_elements[0] + tag + str(count1) + "_prot." + mes_elements[1]
            ecriture(sequence_codon, mon_fichier)
            sequence_codon = []
        count1 += 1


def main():
    
    sequence_fasta = ""

    fichier_input,version, nmax, nmin, start = get_args()

    if nmin == None:
        nmin = 0
    if nmax == None:
        nmax = 1000
    
    fichier_input
    fold_change = version

    count = 0

    with open (fichier_input,"r") as filein:
        for ligne in filein:
            if count > 0 :
                sequence_fasta += ligne.strip("\n")  
            count += 1
    sequence_fasta = sequence_fasta[1:]
 
    #print(sequence_fasta)

    sequence_fasta_rc= rev_complementaire(sequence_fasta)
    ##print(sequence_fasta_rc)


    traduction_cadres(sequence_fasta,fichier_input, "",version, nmax, nmin, start)
    traduction_cadres(sequence_fasta_rc,fichier_input, "rc", version, nmax, nmin, start)


if __name__ == '__main__': # à connaitre
    import sys, argparse # les imports sont ici !
    main() # appel de la fonction
