# smORFind: a prototype software for finding small open reading frames in genomic data

Version: 0.9b  
License: GNU General Public License v3  
Authors: Coulibaly C., Postic G.  
camille.coulibaly@etu.u-paris.fr  
guillaume.postic@u-paris.fr

--------------------------------------

#### 1. INSTALL

Installation only requires cloning the smorfs repository and having Python 3 installed on a UNIX-like operating system.

#### 2. USE

##### Step1:  
Run the `parse_genome.py` script to split the input multi-FASTA genome into individual FASTA files.  
Example:
```
python3 parse_genome.py genomeEcoli.fna
```
This will generate as many `.fna` files as there are sequences in the input file.  
⚠️ The input file must be placed in the same directory as `parse_genome.py`.
##### Step2:
Run the `main.py` script the detect putative ORFs in the input FASTA sequence.  
Example:
```
python3 main.py -i genomeEcoli0.fna
```
This will generate six `_prot.fna` files for the six frames (3'-5' frames are labeled `rc`).  
These files (text format) contain the putative protein sequences, one per line.  

#### 3. OPTIONS
Users can select the minimum and maximum sequence lengths of the output ORFs, as well as the starting residue type, with the following options:
```
-h        show help message and exit
-i        the input multi-FASTA file
-nmax     maximum size threshold of the ORF
-nmin     minimum size threshold of the ORF
-start    choose the first AA of proteins
```
#### 4. WORK IN PROGRESS
* Optimize RAM memory usage to process large genomes
* Compatibility with Windows 10
